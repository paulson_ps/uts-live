jQuery(document).ready(function($) {
  "use strict";

  function validate(ele) {
    var input = $(ele);
    var message = $(ele).val();
    if (message) {
      input.removeClass("invalid").addClass("valid");
      return true;
    } else {
      input.removeClass("valid").addClass("invalid");
      return false;
    }
  }
  function validateEmail(ele) {
    var input = $(ele);
    var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var is_email = re.test(input.val());
    if (is_email) {
      input.removeClass("invalid").addClass("valid");
      return true;
    } else {
      input.removeClass("valid").addClass("invalid");
      return false;
    }
  }

  $(document).ready(function() {
    $("#name").on("input", function() {
      validate(this);
    });
    $("#email").on("input", function() {
      validateEmail(this);
    });
    $("#message").keyup(function(event) {
      validate(this);
    });
    $("#subject").on("input", function() {
      validate(this);
    });

    // <!-- After Form Submitted Validation-->
    $("#submit-form").click("input", function(event) {
      var $form = $("form#contact-form"),
        url = "https://script.google.com/macros/s/AKfycbztML2VRHn8zUO9OggwQKlddcbdE-4RVs42Z1R7ax_J5EtL8Op6/exec";
      var form_data = $("#contact-form").serializeObject();
      event.preventDefault();
      let isvalid = true;
      if (!form_data.name) {
        isvalid = validate($("#name"));
      }
      if (!form_data.email) {
        isvalid = validateEmail($("#email"));
      }
      if (!form_data.message) {
        isvalid = validate($("#message"));
      }
      if (!form_data.subject) {
        isvalid = validate($("#subject"));
      }
      if (!isvalid) {
        return;
      }

      var jqxhr = $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        data: form_data
      })
        .done(r => {
          console.log("success", r);
          $("input[type=text],input[type=email], textarea").val("");
          $(".required").removeClass("valid");
          $(".msg")
            .removeClass("msg-hide")
            .addClass("msg-show")
            .fadeIn("slow")
            .delay(3000)
            .fadeOut("slow");
        })
        .fail(function() {
          alert("Please fill all fields or Check your network");
        });
    });
  });
});

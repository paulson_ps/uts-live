AOS.init({
  duration: 800,
  easing: "slide"
});

(function($) {
  "use strict";
  $(window).stellar({
    responsive: true,
    parallaxBackgrounds: true,
    parallaxElements: true,
    horizontalScrolling: false,
    hideDistantElements: false,
    scrollProperty: "scroll"
  });

  var fullHeight = function() {
    $(".js-fullheight").css("height", $(window).height());
    $(window).resize(function() {
      $(".js-fullheight").css("height", $(window).height());
    });
  };
  fullHeight();

  // loader
  var loader = function() {
    setTimeout(function() {
      if ($("#d4u-loader").length > 0) {
        $("#d4u-loader").removeClass("show");
      }
    }, 1);
  };
  loader();

  // Scrollax
  $.Scrollax();

  var carousel = function() {
    $(".carousel-testimony").owlCarousel({
      center: true,
      loop: true,
      items: 1,
      margin: 30,
      stagePadding: 0,
      nav: false,
      navText: ['<span class="ion-ios-arrow-back">', '<span class="ion-ios-arrow-forward">'],
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 2
        },
        1000: {
          items: 3
        }
      }
    });
  };
  carousel();

  $("nav .dropdown").hover(
    function() {
      var $this = $(this);
      // 	 timer;
      // clearTimeout(timer);
      $this.addClass("show");
      $this.find("> a").attr("aria-expanded", true);
      // $this.find('.dropdown-menu').addClass('animated-fast fadeInUp show');
      $this.find(".dropdown-menu").addClass("show");
    },
    function() {
      var $this = $(this);
      // timer;
      // timer = setTimeout(function(){
      $this.removeClass("show");
      $this.find("> a").attr("aria-expanded", false);
      // $this.find('.dropdown-menu').removeClass('animated-fast fadeInUp show');
      $this.find(".dropdown-menu").removeClass("show");
      // }, 100);
    }
  );

  $("#dropdown04").on("show.bs.dropdown", function() {
    console.log("show");
  });

  // scroll
  var scrollWindow = function() {
    $(window).scroll(function() {
      var $w = $(this),
        st = $w.scrollTop(),
        navbar = $(".d4u_navbar"),
        sd = $(".js-scroll-wrap");

      if (st > 50) {
        if (!navbar.hasClass("scrolled")) {
          navbar.addClass("scrolled");
        }
      }
      if (st < 50) {
        if (navbar.hasClass("scrolled")) {
          navbar.removeClass("scrolled sleep");
        }
      }
      if (st > 350) {
        if (!navbar.hasClass("awake")) {
          navbar.addClass("awake");
        }

        if (sd.length > 0) {
          sd.addClass("sleep");
        }
      }
    });
  };
  scrollWindow();

  var contentWayPoint = function() {
    var i = 0;
    $(".d4u-animate").waypoint(
      function(direction) {
        if (direction === "down" && !$(this.element).hasClass("d4u-animated")) {
          i++;

          $(this.element).addClass("item-animate");
          setTimeout(function() {
            $("body .d4u-animate.item-animate").each(function(k) {
              var el = $(this);
              setTimeout(
                function() {
                  var effect = el.data("animate-effect");
                  if (effect === "fadeIn") {
                    el.addClass("fadeIn d4u-animated");
                  } else if (effect === "fadeInLeft") {
                    el.addClass("fadeInLeft d4u-animated");
                  } else if (effect === "fadeInRight") {
                    el.addClass("fadeInRight d4u-animated");
                  } else {
                    el.addClass("fadeInUp d4u-animated");
                  }
                  el.removeClass("item-animate");
                },
                k * 50,
                "easeInOutExpo"
              );
            });
          }, 100);
        }
      },
      { offset: "95%" }
    );
  };
  contentWayPoint();

  // navigation
  var OnePageNav = function() {
    $(".smoothscroll[href^='#'], #d4u-nav ul li a[href^='#']").on("click", function(e) {
      e.preventDefault();

      var hash = this.hash,
        navToggler = $(".navbar-toggler");
      $("html, body").animate(
        {
          scrollTop: $(hash).offset().top
        },
        700,
        "easeInOutExpo",
        function() {
          window.location.hash = hash;
        }
      );

      if (navToggler.is(":visible")) {
        navToggler.click();
      }
    });
    $("body").on("activate.bs.scrollspy", function() {
      console.log("nice");
    });
  };
  OnePageNav();
})(jQuery);
